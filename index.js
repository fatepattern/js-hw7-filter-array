function filterBy(array, dataType){

  let newArray = [];

  for(let i = 0; i < array.length; i++){
    if(typeof array[i] != dataType){
      newArray.push(array[i]);
    }
  }

  return newArray;

}

let arr = ['hello', 'world', 23, '23', null];

let newArr = filterBy(arr, 'string');

for(let i = 0; i < newArr.length; i++){
  console.log(newArr[i]);
}

console.log(Array.isArray(newArr[1]))